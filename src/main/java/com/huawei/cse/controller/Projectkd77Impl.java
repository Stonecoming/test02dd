package com.huawei.cse.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-01-10T12:19:08.418Z")

@RestSchema(schemaId = "projectkd77")
@RequestMapping(path = "/cse", produces = MediaType.APPLICATION_JSON)
public class Projectkd77Impl {

    @Autowired
    private Projectkd77Delegate userProjectkd77Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userProjectkd77Delegate.helloworld(name);
    }

}
